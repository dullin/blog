---
Some Thoughts on a Compiler API — part I
Jean Privat
2023-03-25
---

# This document is a work in progress

# Some Thoughts on a Compiler API — part I

In most languages, you do not want to be forced to think about its compiler, and you shouldn't.
You just want to write source code and magically get executable blobs (with a very high level of confidence).

However, sometimes pieces of software have to interact with the compiler trough an API (not hyped web API stuff, but just the plain old classes and methods).
It is especially true in Pharo since the compiler is a main (but optional) part of Pharo environments.
For instance, the compiler is used (obviously) by code editors: to compile but also to highlight code and give other kinds of feedback while editing; to install software (Code Importer, Monticello, Iceberg, etc.); to evaluate random text (Do It, evaluate, etc.), to transform and refactor code, etc.

Therefore, what is a good API for all these use cases? Should it be a few independent but simple APIs specific to these usages? A single common API? A complex and powerful API?
This is the kind of questions I try to answer while refactoring (and hopefully improving) the code of the compiler.
The status on the ongoing work is accessible at <https://github.com/pharo-project/pharo/issues/12883>.

## Clients of a Compiler

APIs are designed for clients; therefore, it's better to present the clients before discussing the API.

Here are the four main "jobs" a compiler is used for in Pharo.

* `parse` takes a source code (string) and gives a rich AST (abstract syntax tree).

  We'll enter the details in the next section, but in parse, I also include the semantic analysis because we want the AST to be as rich as possible to be useful.

  Such an AST can be used for source-code level manipulation (refactoring, code transformation, etc.),
  user feedback like highlighting (or error reports),
  or contextual service. For instance, right-click on a colorful part of the code and get a menu with actions tailored for the program element that propose you some handy navigation or refactorizations.

* `compile`

  Transforms some source code (string) into an executable blob (`CompiledMethod` in Pharo).

  The usage of this method is specialized because manipulating "floating" compiled methods implies specific needs.
  The two following jobs are more common but are an extension of the `compile` job.

* `evaluate` transforms some source code (string) into effects.

  This one is used a lot because it's the classic "eval" and because Pharo has powerful "DoIt" contextual actions.

* `install` takes the source code of a method definition (string) and puts it in a class so its instances can invoke it.

  As in most dynamic languages, the newly installed method is then directly available, and existing instances can invoke it.

  Note: in Pharo, this one is not directly available in `OpalCompiler`, the current compiler but is accessible with the family of methods `compile*` of the `ClassDescription` class. We will discuss them later.

  This job is used a lot by editors, loaders, refactoring tools, etc. when installing methods in classes.

## Design of a Compiler

Internally, a compiler is designed mostly as a pipeline of independent steps, usually separated into two parts.

The frontend:

* A lexical analysis (done by a scanner/lexer) that gives a stream of tokens.
* A syntactic analysis (done by a parser) that gives an abstract syntax tree (AST).
* A semantics analysis that annotates nodes of the AST with semantic information (variables, for instance, but what to do exactly and how to do it depends on the language).
  Lucky people even have some kind of metamodel at this point.

At the end of the frontend, we have the output of the `parse` job.

The "not-frontend" (a backend preceded by possible middle-ends):

* Various passes of analysis and transformation to prepare the AST for the next step.
* Generation of some intermediate representation.
* Various passes of analysis and transformation on the intermediate representation (typically to improve performance).
* Generation of some binary final representation (machine code for a real processor or for a virtual machine).

At the end of the pipeline, we have the output of the `compile` job.

But what we care about in this article is not the internal design of the compiler and its internal API, but a nice public API for the four jobs we identified.
What about `evaluate` and `install` by the way? In fact, compiler folks often consider them simple management that doesn't deserve that much attention.

## Designing a public API of Compiler

A simple solution could be to have no special API and just deal with the internal API and the pipeline of components of the compiler yourself.
This might not be the best choice for two reasons:

* The internal API is complex and users will use it badly and in some inconsistent way (and will possibly hurt themselves).
* We want to be able to improve the internal API, and changing or removing internal components without too much hassle.
  If random tools start to access internal things, this will become a maintenance nightmare.
  Specialized tools can still access the internal API but by doing so, they become responsible for staying compatible with the evolution of the internal API.

Another simple solution could be to have only four high-level public methods, one for each job we identified.
While simple, this solution does not lack charm, but the question is, what is the signature of these four methods?
It depends on the various parameters and options required to perform the job.

## Parameters and Options of a Compiler

What information, parameters, or options does each of these four jobs need?

### What is the source code?

This one was easy.
All four jobs need it.

### Is it for a method or a script/expression?

For historical reasons, in Pharo, there are two parsing modes, one for method definition and one for scripts (and expressions).
And you need to know that beforehand because it is ambiguous (heuristics are still possible, but you do not want them in this part of a compiler).

For instance, `foo bar baz` as a method definition means "method named `foo` with a single statement that calls the method `baz` on the variable (possibly attribute) `bar`". e.g., `void foo() { bar.baz(); }` in some random other language.
Whereas, as a script/expression, it means "call `bar` on the variable (possibly attribute) `foo`, then call `baz` on the result" (i.e., `foo.bar().baz()`).

Method or not? It's a simple boolean information, with a possible default value.

* parse: can be equally both;
* compile: it's for specific and aware clients, thus can be both, but method is more likely;
* evaluate: for scripts/expression — evaluating a method definition is possible, but the need seems rare;
* install: for method definition — installing a script as a method is possible, but the need seems rare, and a method name (selector) is required as shown further.


## What is the scope/context?

In Pharo, it is used to know about variables.
That includes reserved variable like `self`, instance variable (attributes), shared variables, global variable, etc.
And a scope can be a lot of things (unfortunately).

* The simple answer is "none/don't care". In Pharo, it means that the receiver is `nil` and the class is `nil class` (called `UndefinedObject`).
* A standard answer is a class, especially when you compile a method.
* A complex answer is a `Context` (i.e. a live stack frame with actual live variables and objects).
  It's used in the debugger, for instance.
* A more complex answer is whatever you want.
  For instance, the *workspace* (the playground, a simple but handy semi-interactive text area) have special *workspace variables* that cause a lot of suffering for compiler developers.

This scope information is needed for all four jobs, including parse.
For instance we want to correctly highlight code and offer powerful contextual actions in the workspace (script with magic workspace variables), in an code editor open on a method of a class or in a debugger while inspecting stack frames.

### What Compilation Optimization to Activate (or Deactivate)?

This parameter is obviously not useful for `parse`.

They also have default values (possibly configurable in the system settings).
And can even be dictated by a special *pragma* (some kind of annotation) in the method definition.

### Where to install the method?

This parameter only makes sense for the `install` job. What is really needed is:

* A class. But we might already have it thanks to the scope parameter.
* A selector, that is already present in the source code of a method definition.
* A protocol (category).

  In Pharo, a method belongs to a group of methods called "protocol", it's only an organization thing.
  We do not already have this one.

* A change stamp. We also do not have this one.

  It will be used to log the new method in a special log file and in the image source file.
  The protocol, the selector, and the class are also required.
  The original source code is also required, but we still have it (don't we?).
  The point of these files is out of the scope of the present article.

  A default change stamp is possible.
  Note that this log-thing is also optional, so another boolean parameter?

* A silent flag.

  Installing a method in a class is usually announced system-wide.
  For instance, it allows class browser windows to automatically update their contents.
  But sometimes (mainly for unit tests), we do not want to do system-wide announcement, so another boolean parameter?

## Starting designing the API

### Method Parameters

There is a lot of parameter and options, and most are shared among the four jobs.
So a first approach could be 4 methods with some insane numbers of parameters.

What about default parameters? Yes, except that there is no such a feature in Pharo.
What is possible is a family of methods to cover the combination of parameters (or only the popular combinations).
An example is the family "compile*" of `ClassDescription` that groups 8 methods.
The longest one being `ClassDescription>>#compile:classified:withStamp:notifying:logSource:` (it's the way to designate method in Pharo) that still lack some parameters like the optimization options or the silent flag.
The class (for scope an installation target) is the receiver.

It is used this way:

```st
aClass	compile: aSource
	classified: aProtocol
	withStamp: aStamp
	notifying: aRequestor
	logSource: aBoolean
```

Note about the Pharo syntax: it is a **single** method call.
Just imagine that its like some language you know by that all parameters are named and that parenthesis and commas use some invisible color.
Line breaks are added for visibility and are just blanks in Pharo anyway.
For instance, the equivalent in a random fictive language could be:
```c#
aClass.compile(aSource,
	classified: aProtocol,
	withStamp: aStamp,
	notifying: aRequestor,
	logSource: aBoolean);
```

Personally, I do not like these kind of method families, and here we are dealing with an important numbers of parameters. This feel limiting, because you might not know what combinations are really available and adding a new parameter worsen the problem.

Moreover, you must know all the parameter right now and cannot easily prepare the call with some parameters then delegate the rest of the job, nor get a pre-populated set of parameters.


### Collection of Options

Another approach could be to pass an collection of options and parameters, in a Dictionary (HashMap) for instance.
This could give something like:

```st
aClass compile: {
	#source -> aSource.
	#classified -> aProtocol.
	#withStamp -> aStamp.
	#notifying -> aRequestor.
	#logSource -> aBoolean }
```

Syntactic note: `#` are for symbols (interned strings), `{}` are for arrays (elements separated with dots `.`) and the arrow `->` is the common association operator. But this detail is not important.

Beside the ugly syntax, the main issue is the lack of reification: manipulating random values associated to random strings is error prone and IDE will be likely unhelpful.
Think about typos in keynames or the limited completion.

But it is flexible: you can easily add or remove new parameters.
The preparation of the collection can be delegated and passed between methods.
And there is a single powerful `compile:` method not a limited family.

### Reify Options

We can reify the collection of options as a specific class, and get a nice independent API to setup options and parameters some with simple basic setters/getters and some other involving rich methods and internal state.

Pharo offers a thing called `cascade` to have nice fluent API.
They are used to chain calls on the same receiver and the syntax is a semi-column (`;`).
The equivalent in other language is to return `self` (or `this`) for each method and simply chain method calls.

This could get you:

```st
aClass compile: (CompilerOption new
	source: aSource;
	classified: aProtocol;
	withStamp: aStamp;
	notifying: aRequestor;
	logSource: aBoolean)
```

It looks like a little like the first proposal (the one with many parameters), but with the benefits of the second one (the collection).
Still is a single `compile:` method (not a family) with a single rich parameter.
The configuration of this parameter use a sequence of (fluent) setters that are real methods and the option is a real object.

Could we do better?

### Assign Responsibility

This option class feels limited.
Pure data classes are a code smell because good designers assign responsibilities to classes that have the data or move data to classes that have the responsibilities.

So, let's give it the responsibility to do the jobs, thus give it a name that fits better.
Here's what that might look like:

```st
Compiler new
	class: aClass;
	source: aSource;
	classified: aProtocol;
	withStamp: aStamp;
	notifying: aRequestor;
	logSource: aBoolean;
	install
```

We might have something here.

A `Compiler` class that we can instantiate and prepare to perform some final action, which in this case is the "install" job.
And this is exactly the current design of the public API of `OpalCompiler`, the current compiler of Pharo, except that Pharo11 only has the `parse`, `compile`, and `evaluate` jobs, and not the `install` job. `install` is assigned to the limited `compile*` family of methods of the `ClassDescription` class.
Pharo12 did gain the install method, but the API is still improving, so the example might not works as is.

Instance of `OpalCompiler` class can also be prepared in advance.
The `compiler` method of the `Behavior` class (a superclass of `ClassDescription`) gives a compiler instance preconfigured with the class as the scope.

So, since in Pharo12 the compiler gained the `install` responsibility, you can have something like:

```st
aClass compiler
	source: aSource;
	classified: aProtocol;
	withStamp: aStamp;
	notifying: aRequestor;
	logSource: aBoolean;
	install
```

Simpler usages are also possible thanks to default options and rich methods of the `OpalCompiler` class.
For instance, the following gives a (semantic) AST for a method definition source code (`aSource`) analyzed in the scope of `aClass`.

```st
anAST := aClass compiler parse: aSource
```

Here `parse:` is an helper method that does the `parse` job with the given source as parameter.
It combines `source:` and `parse`, and this one example exits is available in previous version of Pharo.

So, is everything really nice in the opal world?
What about these weird `notifying: aRequestor` parameters in the previous examples?
And, what do we do when things go wrong and errors need to be managed?

This will be in the second part of the article :)
