---
Some Thoughts on a Compiler API — Part II
Jean Privat
2023-03-25
---

# This document is a work in progress

# Some Thoughts on a Compiler API — Part II

We have a nice API for compiling methods.

But what about errors?

A special difficulty is the handling of errors, and in a compiler you want to get errors in order to present them to the user and because you want to control what to do with bad code.

Note that by errors, we focuses on errors in the source code, not internal errors of the compiler (because all compilers a bugfree, ahahaha) or usage error related to a misuse of the API.
Therefore the term "error" is not the right one.

In Pharo11, the handling of errors during the can be quite complex and spaghettids with some pingpong of notification (see previous articles "Undeclared Variable Reparation, An Epic Journey In a Compiler").

This is part, we try to do "fresh" start on the design, and see what we can get.

## Classic Error Handling

There is some traditional way to deal with errors, and it's often better to relying on these traditional way to avoid misunderstanding of users.

By users, I mean developers who program software that deal with the compiler.
Those software programs might be used by (others) developers that do not care about the compiler but want to program other kind of software.
And those other kinds of software programs might target some real final users.

### Special Return Value

Either are a replacement of the expected value, or as an additional return value.
Languages and core libraries offers various way to deal with these approaches. Going from `-1` in classic C API to the `Either` type in Haskell, for instance.

I have a very limited experience in Pharo, but I did not see a lot of these kinds of error management.

### Exception

Disclaimer: I do not like exceptions, for a lot of reasons, but I will try to stay neutral.

They offer a simple way to dynamically bind "error situation" in some part of the code to "error handling" in other part of the code.
One of these drawbacks is that they are broad.

When you catch an exception, you don't really know what is the specific source and what is the specific state of the system.

Some language force you to declare (some) exceptions to limit these issues.

### Error Block (or Lambda/Function)

Give some simple callback to call when something bad happens.
This one is used a lot in Pharo, especially in the core library.
For instance, `at:ifAbsent:` of `Dictionary` that evaluates the `ifAbsent` block if the requested element is absent (whereas the simple `at:` just signals an exception).

One little issue with this one is that it's not always clear what is the return value of the whole call.
It depends on the specific method, most of the time the value of the expression is the value of the error block, therefore the client can choose which "default" value to use.
On the implementation side, it's easy to do for simple methods.
On complex situation with deep call chains and many classes and method involved, the "return journey" of the error value can be tricky.

### Heavy Callback

Instead of poor anonymous block/lambda/function, why not attaching reified error handler as a full object/value with a specific and rich interface/type and a rich state that will be invoked in for error handling.
Most advantages and drawback are the same from the previous point.

The main specific advantage is the richness of the object/value used, that can offer, for instance, multiple callback methods that can handle multiple error situations, a state that persists between these callbacks, and good documentation (ahahah).
The drawback is that it is heavy.
Each client might have to design a specific callback compatible object, and be able to communicate with it.

Some languages offer facilities like anonymous class instantiation, or just accept (promote?) heavy design.
Heavy callback is not a bad design, but I believe that heaviness of the solution should be proportional to the complexity of the issue to address, and that, a la Occam's razor, between and heavy design and a simple but equivalent one, chose the simple one (the devil's detail is in the definition of *equivalent*).

### Domain Expansio[n](https://en.wikipedia.org/wiki/Jujutsu_Kaisen)

Expand the domain of the result/return value and reify faulty or broken results.

Like *special return value*, the error information if given within the result.
But these two approaches should nor be mistaken.

The main advantage is that you do not have to deal with error handling right now: errors are no more errors, they are just data.
This makes the API simpler, especially in complex software architectures where you delegate to something that will also delegate to something else.
It also permits the clients to have specific error management according to their need (or just pass the hot error potato to their own client).

The main drawback is that clients have to deal with an expanded result domain.
Another drawback is that while this simplifies the design of functions/methods, it can make the design of data far more complex.


### Combination

All these kinds of error managements are not exclusive and can be used together.

It can be to have a kind of error handling specific to each kind or error.
So that the most efficient approach is used.

If can be to have multiple kinds of error handling for a single error situation.
This way the client can use the one they prefer.
A classic example is Dictionary where the absence of key is handled differently: `at:` signals an exception and `at:ifAbsent:` evaluates a block.

They can also be nested like we saw in a previous article in the method `OpalCompiler#>>compile`.

* a syntax error the signals `SyntaxErrorNotification` (exception);
* the exception is caught by the compiler that invokes a method on the requestor (heavy callback);
* after that, a failBlock is invoked to terminate the call (error block);
* the value of the failBlock is used as the return value of the `compile` method (return value).

I won't advocate too much about combining the error managements, they make the API confusing and the code hard to maintain.

## The Four Jobs of the ~~Apocalypse~~ Compilation

We have `parse`, `compile`, `evaluate`, and `install`.
Look at the previous part for the details.

Do they they produce the same kind of error?
What are the kind of error handling that clients might like?

### `parse`

A client parse some source code, and there is errors, what does it ideally wants?
It want an AST with all error information included: information about syntax and semantic error, so it could present the whole non-error source code with most information and highlight errors red, put a danger/warning icon the the margin, propose quick fix on a right-click, etc.

Therefore, the best error handling here is the domain expansion.
In fact, other kind of error management will be a burden here.
What can be done with an exception "*error line 4, unexpected token `]`*" and no AST is indeed limited.

The drawback is that domain expansion on AST need a lot of work.
It means having some kind of parsing and semantic error recovery and able to work on degraded mode.
But if this greatly increase the usefulness of the `parse` job, doing the work is meaningful.

### `compile`

Except for internal errors, there should be no possible backed error.
All issues related to a possible "non-compilation" must have be caught at the parse job and reportable to the final user.
These are not syntactic of semantic error, but compilation limitation "sorry, I cannot compile this because of that".

So no new error to catch, this simplifies the design.

The result of `compile` is an executable blob, its format and semantic is imposed by the virtual machine.
Therefore this limits a lot the compatibility with domain expansion.

There are still some ways to include some side-channel metadata.
One of the simpler, if fact, is to attach the AST (a domain expanded one, with error included) to the compiled method, and generate machine code that fails at runtime.
Clients should beware, but it is the issue with domain expansion.

Other error handling approaches have no specific constraints, so they could be a better choice.

### `evaluate`

This job is a little tricky because `evaluate` does two things: compile the code, then execute it.

The code execution can cause any kind of errors by itself and we want to be able to distinguish then the from errors in the source code.
Therefore, this basically prohibit use of broad error handling like exceptions.

Moreover, the return value of the evaluate is the result of the evaluation, that can be anything.
Therefore, this basically limits the use of value based error handling like special return values of domain expansion — in fact multiple return values or a `Either` type can do the trick, but these approaches seems very rare in Pharo.

The only kind that remain are callback based approach (light or heavy).

### `install`

Theoretically, install might provide new kinds of errors because it does some system-wide operations that could be forbidden or impossible for a lot of reasons.
However, Pharo is very permissive and allows you to break your system quite easily.

Moreover, all these errors could have been caught at `parse`-time because all relevant information is already available: the class, the selector and the source code of the method.
And because the final user might want to know that the current edited method will not be installable.

Note that at the API level, current `install` job (called `compile` in the `ClassDescription` class to add confusion) returns the the selector (the name, a symbol) of the method.
The name cannot be used with domain expansion (a symbol is a symbol!).
However, a lot of client directly use the selector to retrieve the compiled method, and we are in the situation of the `compile` job.

But it's not the real issue with the `install` job.
The return value is not that useful because what is useful are the side effects: it installs a method system-wide.
So what the client really wants is "*was the method correctly installed, and it not, why?*".
Asking the client to check the return value might not be the best way.
Exceptions or callbacks seem to fit better the situation.

## Towards a Proposal

All four jobs seems to have different constraints on error handling.
A possible solution could be to offer four distinct approaches.
But this will cause confusion on user.
So let's stash this as a B plan.

Here a summary on the constraints and preferences:

* Pharo do not favor special return values;
* `parse` prefers domain expansions;
* `compile` prefers all except domain expansions;
* `evaluate` can only accept callbacks;
* `install` prefers exceptions or callbacks.

So callback it is? But light or heavy?

And what about parse?
We can still provide domain expansion and have extended AST with error information, for this job.

### Back to the Reality

We already show that current (Pharo11) `OpalCompiler` offers a complex set of rules to handle error situation.

* The `requestor` (heavy callback). It has no real interface, breaches a lot of encapsulation, and its usage is very fragile.
* The `failBlock` (block callback). Is only used to fast exit a call to the compiler (no local return) if a requestor is present, it does not even have any parameter.
* Exceptions. They are signaled too early at the first syntactic error (so an AST is not available) or semantic error (so the AST is partially analyzed).
  They are also badly caught in the case of the `evaluate` job and cause a lot of confusion.
* `optionParseErrors`, `optionParseErrorsNonInteractiveOnly` and `optionSkipSemanticWarnings` are boolean flag used to toggle some error handling, especially preventing the signal of some exceptions.

### How to improve things

Here the plan I'm currently trying to implement in the early cycle of Pharo12 (some work was included at the end the the Pharo11 cycle).

* Domain expansion on the AST.
  The only result of `parse` is an AST, with possible error information on it.
  Error (and warning) information are reified as `RBNotice` attached to the nodes.
  No early exits, no exceptions, the full parsing and the full sematic analysis is always done.
* Remove the `requestor`. Only keep it as a "quirks mode" to be compatible with existing clients.
* For the three other jobs, block callback, with `RBNotice` instances as a parameter of the block, so specific error information is available with the same rich object used to decorate AST.
  Maybe reuse the existing `failBlock` and gives it a optional parameter (the notice).
* In the absence of callback (or if the callBack returns), a generic error will be signaled (preventing clients to rely too much on exceptions).
* remove `optionParseErrors`, `optionParseErrorsNonInteractiveOnly` and `optionSkipSemanticWarnings` and replace them with a simple `permitFaulty` flag that disable `failBlock` (and exceptions).
  In fact, the flag default to `true` for the parse job and to false for the other jobs, making the code of the error handling independent of the specific performed job (just look at the faulty flag)

Some design questions are yet to answer:

* is the `fallBlock` called for each error, or only for the first one?
* can the `failBlock` choose to resume some errors? so be used as a an "error" filter.

The use-cases I have in mind are to permit compilation or installation of some methods that contains errors (I'm thinking specifically about undeclared variables), thus the client could be able to choose on which error situation means to abort.
But maybe it is a bad idea to put too much responsibility on the clients hand.
An alternative approach could be to transform the boolean `permitFaulty` into a discrete `faultyLevels` that groups various level of code issues, allowing some and rejecting others.
